const Tokenizer = require("../tokenizer");

let tokenizer = new Tokenizer(`
  wczytaj comes::obiekty
  wczytaj grafika::obrazek

  ustal ustawienia(O())
  O(ustawienia, "szerokość", 800)
  O(ustawienia, "wysokość", 600)

  ustal obrazek(NowyObrazek(ustawienia))

  ustal farba(NowaFarba())
  KolorWypełnienia(farba, Kolor(255, 255, 255))

  RysujProstokąt(obrazek, farba, 32, 32, 64, 64)
`);
let tokens = tokenizer.tokenize();

console.log(tokens);
