const Tokenizer = require("../tokenizer");
const Parser = require("../parser");

let tokenizer = new Tokenizer(`
  metoda X
    ustal WYNIK(Pl(*1, 3))
  koniec
`);
let tokens = tokenizer.tokenize();
console.log(tokens);
let parser = new Parser(tokens);
console.dir(parser.parse(), { depth: null });
