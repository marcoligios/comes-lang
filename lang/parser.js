class Parser {
  constructor(tokens) {
    this.tokens = tokens;
    this.pos = 0;
  }

  error(message) {
    let pos = this.peek().pos;
    throw new SyntaxError(`${pos.ln}:${pos.col} ${message}`);
  }

  get atEnd() {
    return this.pos >= this.tokens.length;
  }

  peek(offset = 0) {
    if (this.pos + offset < this.tokens.length) {
      return this.tokens[this.pos + offset];
    } else {
      return { type: "comes huj" }
    }
  }

  next() {
    this.pos++;
  }

  consume() {
    let tok = this.peek();
    this.next();
    return tok;
  }

  match(type) {
    if (this.peek().type == type) {
      this.next();
      return true;
    }
    return false;
  }

  lit() {
    switch (this.peek().type) {
      case "kwNull":  return { type: "null" };
      case "kwTrue":  return { type: "bool", bool: true };
      case "kwFalse": return { type: "bool", bool: false };
      case "number":  return { type: "number", num: this.consume().num };
      case "string":  return { type: "string", str: this.consume().str };
    }
  }

  ref() {
    if (this.peek().type == "ident") {
      return { type: "ref", variable: this.consume().ident }
    }
  }

  arg() {
    if (this.match("punctStar")) {
      if (this.peek().type == "number") {
        return { type: "arg", id: this.consume().num };
      } else {
        this.error("Oczekiwano numeru argumentu !");
      }
    }
  }

  call() {
    if (this.peek().type == "ident") {
      let name = this.consume();
      if (this.match("lPar")) {
        let args = [];
        while (true) {
          if (this.atEnd) this.error("Brakujący prawy nawias !");
          let arg = this.parse("expr");
          if (arg != null) args.push(arg);
          if (this.match("punctComma")) continue;
          if (this.match("rPar")) break;
          this.error("Zła składnia wywołania !");
        }
        return { type: "call", method: name.ident, args };
      }
    }
  }

  expr() {
    let expr = this.parse("lit");
    if (!expr) expr = this.parse("call");
    if (!expr) expr = this.parse("ref");
    if (!expr) expr = this.parse("arg");
    return expr;
  }

  doStmt() {
    if (this.match("kwDo")) {
      let body = [];
      while (!this.match("kwEnd")) {
        let stmt = this.parse("stmt");
        if (!stmt) this.error("Oczekiwano zdania !");
        body.push(stmt);
      }
      return { type: "do", body };
    }
  }

  varOp() {
    let op;
    if (this.match("kwLet")) op = "let";
    else if (this.match("kwSet")) op = "set";
    if (op) {
      if (this.peek().type == "ident") {
        let name = this.consume();
        if (!this.match("lPar")) this.error("Oczekiwano lewego nawiasu !");
        let val = this.parse("expr");
        if (!this.match("rPar")) this.error("Oczekiwano prawego nawiasu !");
        return { type: op, name: name.ident, val }
      } else {
        this.error("Oczekiwano nazwy zmiennej !");
      }
    }
  }

  ifStmt() {
    if (this.match("kwIf")) {
      let cond = this.parse("expr");
      let body = [];
      while (!this.match("kwEnd")) {
        let stmt = this.parse("stmt");
        if (!stmt) this.error("Oczekiwano zdania !");
        body.push(stmt);
      }
      return { type: "if", cond, body };
    }
  }

  whileLoop() {
    if (this.match("kwWhile")) {
      let cond = this.parse("expr");
      let body = [];
      while (!this.match("kwEnd")) {
        let stmt = this.parse("stmt");
        if (!stmt) this.error("Oczekiwano zdania !");
        body.push(stmt);
      }
      return { type: "while", cond, body };
    }
  }

  forLoop() {
    const comma = () => {
      // nie da sie uzyc 'function' bo to kurestwo zmienia mi 'this'
      // musze wiec wygladac jak typowy webdebiloper fanatyk FP zeby to gowno
      // dzialalo
      // pierdolic JS :middle_finger:
      if (!this.match("punctComma")) this.error("Oczekiwano przecinka !");
    }
    if (this.match("kwFor")) {
      if (!this.match("lPar")) this.error("Oczekiwano lewego nawiasu !");
      if (this.peek().type != "ident") this.error("Oczekiwano nazwy zmiennej !");
      let varName = this.consume().ident; comma();
      let min = this.parse("expr"); comma();
      let max = this.parse("expr"); comma();
      let inc = this.parse("expr");
      if (!this.match("rPar")) this.error("Oczekiwano prawego nawiasu !");
      let body = [];
      while (!this.match("kwEnd")) {
        let stmt = this.parse("stmt");
        if (!stmt) this.error("Oczekiwano zdania !");
        body.push(stmt);
      }
      return { type: "for", variable: varName, min, max, inc, body };
    }
  }

  method() {
    if (this.match("kwMethod")) {
      if (this.peek().type == "ident") {
        let name = this.consume().ident;
        let body = [];
        while (!this.match("kwEnd")) {
          let stmt = this.parse("stmt");
          if (!stmt) this.error("Oczekiwano zdania !");
          body.push(stmt);
        }
        return { type: "method", name, body: { type: "do", body } };
      } else {
        this.error("Oczekiwano nazwy metody !");
      }
    }
  }

  importStmt() {
    if (this.match("kwImport")) {
      if (this.peek().type == "ident") {
        let namespace = [this.consume().ident];
        while (this.match("punctDColon")) {
          if (this.peek().type == "ident") {
            namespace.push(this.consume().ident)
          } else {
            this.error("Oczekiwano przestrzeni nazw !");
          }
        }
        return { type: "import", namespace: namespace.join("::") };
      } else {
        this.error("Oczekiwano przestrzeni nazw !");
      }
    }
  }

  stmt() {
    let stmt = this.parse("expr");
    if (!stmt) stmt = this.parse("varOp");
    if (!stmt) stmt = this.parse("ifStmt");
    if (!stmt) stmt = this.parse("whileLoop");
    if (!stmt) stmt = this.parse("forLoop");
    if (!stmt) stmt = this.parse("method");
    if (!stmt) stmt = this.parse("doStmt");
    if (!stmt) stmt = this.parse("importStmt");
    return stmt;
  }

  script() {
    let script = [];
    while (!this.atEnd) {
      let stmt = this.parse("stmt");
      if (!stmt) this.error("Oczekiwano zdania !")
      script.push(stmt);
    }
    return { type: "script", script };
  }

  parse(kind = "script") {
    let pos = this.pos;
    let node = this[kind]();
    if (!node) {
      this.pos = pos;
    } else {
      return node;
    }
  }
}

module.exports = Parser;
