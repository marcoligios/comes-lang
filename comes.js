console.log("comes.js discord bot");
console.log("made by iLiquid");
console.log("--------------------");

const config = require("./config.json");

const Discord = require("discord.js");
const comes = require("./lang/index");

const client = new Discord.Client();
const tag = "<@568753760302792705>";

function respond(msg, text) {
  msg.channel.send(text);
}

function indent(text, level) {
  let result = [];
  for (let line of text.split('\n')) {
    result.push(' '.repeat(level) + line);
  }
  return result.join('\n');
}

client.on("ready", () => {
  console.log("logged in successfully");
});

client.on("message", (msg) => {
  if (msg.content == tag) {
    respond(msg, comes.changelog);
  }
  if (msg.content.startsWith("»")) {
    let code = msg.content.substring(1);
    console.log(`evaluating from ${msg.author.tag}:`)
    console.log(indent(code, 2))
    let vm = new comes.vm.VM();
    let start = Date.now();
    vm.onNodeEnd = () => {
      let now = Date.now();
      if (now - start > config.evalTimeout) {
        throw new EvalError("Egzekucja zajęła zbyt długo !");
      }
    };
    try {
      vm.eval(".comesjs::main", code);
      if (vm.stdout) {
        respond(msg, vm.stdout);
      } else {
        respond(msg, "<O. K.>");
      }
    } catch (err) {
      console.error(err);
      respond(msg, `\`\`\`${err.name}: ${err.message}\`\`\``);
    }
  }
});

const auth = require("./auth.json");
client.login(auth.token);
