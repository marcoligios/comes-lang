# comes/lang

Język programowania. **Do dupy.**
*Designed by iLiquid and friends™*

## Bot

[Zaproś na swój serwer](https://discordapp.com/oauth2/authorize?client_id=568753760302792705&scope=bot&permissions=1)

Aby ewaluować skrypt w języku comes/lang, wystarczy użyć prefixu `»`
wraz z kodem który ma zostać wywołany:

    »[ Pisz kod tutaj. ]

### Dodatkowe komendy

Nie ma.

## Składnia

Znaki diakrytyczne w słowach kluczowych są opcjonalne, więc zamiast `jeżeli`
można pisać `jezeli`, itd.

### Komentarze...

```comes
[ ...to są po prostu komentarze. ]
```

Jak widać, składnia komentarzy w comes/lang jest bardzo prosta.

### Typy prymitywne

Comes/lang oferuje typowe typy primitywne:
```comes
NIC                  [ Brak wartości "null" ]
TAK PRAWDA NIE FAŁSZ [ Typy logiczne ]
3.1415926            [ Liczby ]
"Cześć!"             [ Ciągi znaków ]
```

### Wywoływanie metod

Aby wywołać metodę, należy użyć składni znanej z C oraz pochodnych:

```comes
Drukuj("Cześć !")
```

### Zmienne

Zmienne deklarowane są podobnie do języka C++:

```comes
[ Najpierw zmienną należy ustalić: ]
ustal x(10)
[ Potem można ją zmodyfikować: ]
ustaw x(15)
[ Oraz się do niej odnieść: ]
Drukuj(x)
```

#### Zasięg zmiennych

Zmienne mają swój zasięg, tak jak w innych językach programowania.

```comes
ustal x(10)
rób
  Drukuj(x) [ Działa ]
  ustal y(20)
koniec
Drukuj(y) [ Nie działa, ponieważ zmienna jest widoczna tylko w bloku 'rób' ]
```

### Kontrola przepływu

Czyli kontrolowanie czy coś ma się wydarzyć czy nie.

#### Zdania `jeżeli`

Są one bardzo proste, składają się ze słowa kluczowego `jeżeli` oraz ekspresji
która ma być ewaluowana. Jeśli ekspresja jest prawdziwa, blok jest wykonywany.

```comes
jeżeli Mn(2, 3)
  Drukuj("To prawda!")
koniec

jeżeli Wi(3, 2)
  Drukuj("Niemożliwe!")
koniec
```

#### Pętle `kiedy`

Pętle te wykonują swój blok tak długo jak ich ekspresja jest prawdziwa.

```comes
ustal i(1)
kiedy Mn(i, 10)
  Drukuj(i)
  ustaw i(Pl(i, 1))
koniec
```

#### Pętle `dla`

Pętle dla pozwalają na proste liczenie.

```comes
dla(i, 0, 10, 1)
  Drukuj(i)
koniec

[ Z powodu zasięgu zmiennych, można tutaj użyć nazwy 'i' po raz drugi. ]
dla(i, 10, 0, -1) [ Można też mieć pętlę odwrotną. ]
  Drukuj(i)
koniec
```

### Metody

```comes
wczytaj comes::zapewnienia

metoda Dodaj2
  ustal WYNIK(Pl(*1, 1)) [ *1 to argument 1 ]
koniec

ustal x(Dodaj2(10))
Zapewnij_TAK(Ró(x, 12))
```

### Moduły

```comes
[ Aby utworzyć moduł, najpierw należy zadeklarować dostępne w nim metody. ]

ustaw x(2)

metoda ZdobądźX
  ustal WYNIK(x)
koniec

[ Następnie należy zapisać moduł. ]

Moduł("zdobywanie::x")

[ Metody utworzone po wywołaniu metody Moduł() nie są widoczne w innych
  modułach. ]

metoda UstawX
  ustaw x(*1)
koniec
```

W innym module…

```comes
wczytaj zdobywanie::x

Drukuj(ZdobądźX())
```

## Biblioteka standardowa

W bibliotece standardowej comes/lang dostępne jest kilka modułów, wszystkie pod
przestrzenią nazw `comes`.

 - `comes::system::matematyka`: Operacje matematyczne
   - `Pl`, `Mi`, `Ra`, `Pr` `(...arg)`: plus, minus, razy, (podzielić) przez
   - `Ró`, `Nró`, `Wi`, `Mn`, `WiR`, `MnR` `(a, b)`: równe, nierówne, większe,
     mniejsze, większe lub równe, mniejsze lub równe
   - `I`, `Lub` `(a, b)`: operacje logiczne AND i OR
   - `Pot` `(a, b)`: potęguj
   - `Modulo`, `Bezwzględna` `(a, b)`
   - `Sinus`, `Kosinus`, `Tangens` `(a)`: funkcje trygonometryczne
 - `comes::system::konsola`: Wyświetlanie rezultatów programu
   - `Drukuj`, `Pisz` `(...wiadomość)`: drukuj wiadomość
 - `comes::system::tekst`: Zarządzanie ciągami znaków
   - `Łącz` `(...tekst)`: łączy dwa ciągi znaków
 - `comes::system::mod`: Moduły
   - `Moduł` `(nazwa)`: zapisuje obecny moduł


Więcej modułów **już wkrótce!** Jeśli chciałbyś zaproponować moduł, napisz
Problem (zakładka Issues).
